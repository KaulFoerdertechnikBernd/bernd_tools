﻿Imports System.Runtime.InteropServices
Imports System.Reflection

Imports SolidWorks.Interop.sldworks
Imports SolidWorks.Interop.swconst
Imports SolidWorks.Interop.swpublished
Imports SolidWorksTools.File

<Guid("ffd70867-0b63-4267-a18c-30059cf80ee4")>
<ComVisible(True)>
Public Class Main
    Implements SwAddin

#Region "Local Variables"

    Private mSWApp As SldWorks
    Private mCookie As Integer = 0
    Private mCommandManager As CommandManager
    Private mBitmapHandler As BitmapHandler


    ReadOnly Property App() As SldWorks
        Get
            Return mSWApp
        End Get
    End Property

    ReadOnly Property CommandManager() As CommandManager
        Get
            Return mCommandManager
        End Get
    End Property

#End Region

#Region "Addin Implementation"

    Public Function ConnectToSW(ThisSW As Object, Cookie As Integer) As Boolean Implements ISwAddin.ConnectToSW
        mSWApp = ThisSW
        mCookie = Cookie

        'Dim smallImage As Integer
        'Dim mediumImage As Integer
        'Dim largeImage As Integer
        'Dim imageSizeToUse As Integer

        'imageSizeToUse = mSWApp.GetImageSize(smallImage, mediumImage, largeImage)

        'MsgBox("Image sizes:")
        'MsgBox("  Default PropertyManager page and menu icon size based on DPI setting: " & imageSizeToUse)
        'MsgBox("    Small image size based on DPI setting: " & smallImage) '20
        'MsgBox("    Medium image size based on DPI setting: " & mediumImage) '32
        'MsgBox("    Large image size based on DPI setting: " & largeImage) '40


        mSWApp.SetAddinCallbackInfo2(0, Me, mCookie)

        'CreateUI()

        mCommandManager = mSWApp.GetCommandManager(mCookie)

        AddCommandManager()

        Return True
    End Function

    Public Function DisconnectFromSW() As Boolean Implements ISwAddin.DisconnectFromSW

        'RemoveUI()

        RemoveCommandMgr()

        Marshal.ReleaseComObject(mSWApp)
        GC.Collect()
        GC.WaitForPendingFinalizers()
        GC.Collect()
        GC.WaitForPendingFinalizers()
        Return True
    End Function

#End Region

#Region "SolidWorks Registration"

    <ComRegisterFunction()>
    Private Shared Sub RegisterFunction(t As Type)
        Dim hklm As Microsoft.Win32.RegistryKey = Microsoft.Win32.Registry.LocalMachine
        Dim hkcu As Microsoft.Win32.RegistryKey = Microsoft.Win32.Registry.CurrentUser
        Dim keyname As String = "SOFTWARE\SolidWorks\Addins\{" + t.GUID.ToString() + "}"
        Dim addinkey As Microsoft.Win32.RegistryKey = hklm.CreateSubKey(keyname)
        addinkey.SetValue(Nothing, 1)
        addinkey.SetValue("Description", "Alle Tools die wir bei Kaul Fördertechnik brauchen")
        addinkey.SetValue("Title", "Kaul Tools")
        keyname = "Software\SolidWorks\AddInsStartup\{" + t.GUID.ToString() + "}"
        addinkey = hkcu.CreateSubKey(keyname)
        addinkey.SetValue(Nothing, 1)
    End Sub

    <ComUnregisterFunction()>
    Private Shared Sub UnregisterFunction(t As Type)
        Dim hkcu As Microsoft.Win32.RegistryKey = Microsoft.Win32.Registry.CurrentUser
        Dim keyname As String = "SOFTWARE\SolidWorks\AddinsStartup\{" + t.GUID.ToString() + "}"
        hkcu.DeleteSubKey(keyname)
        Dim hklm As Microsoft.Win32.RegistryKey = Microsoft.Win32.Registry.LocalMachine
        keyname = "SOFTWARE\SolidWorks\Addins\{" + t.GUID.ToString() + "}"
        hklm.DeleteSubKey(keyname)
    End Sub

#End Region

#Region "UI Methods"
    Private Sub CreateUI()
        mSWApp.SetAddinCallbackInfo(0, Me, mCookie)

        Dim images = New String(2) {Nothing, Nothing, Nothing}

        mSWApp.AddMenu(swDocumentTypes_e.swDocNONE, MenuItems.MenuName, 0)

        mSWApp.AddMenu(swDocumentTypes_e.swDocPART, MenuItems.MenuName, 0)
        mSWApp.AddMenuItem5(swDocumentTypes_e.swDocPART, mCookie, MenuItems.MenuItem1Name + "@" + MenuItems.MenuName, -1, "RunMenuCommand(" + MenuItems.MenuItem1Number.ToString + ")", 1, MenuItems.MenuItem1Hint, images)
        mSWApp.AddMenuItem5(swDocumentTypes_e.swDocPART, mCookie, MenuItems.MenuItem2Name + "@" + MenuItems.MenuName, -1, "RunMenuCommand(" + MenuItems.MenuItem2Number.ToString + ")", 1, MenuItems.MenuItem2Hint, images)

        mSWApp.AddMenu(swDocumentTypes_e.swDocASSEMBLY, MenuItems.MenuName, 0)
        mSWApp.AddMenuItem5(swDocumentTypes_e.swDocASSEMBLY, mCookie, MenuItems.MenuItem2Name + "@" + MenuItems.MenuName, -1, "RunMenuCommand(" + MenuItems.MenuItem2Number.ToString + ")", 1, MenuItems.MenuItem2Hint, images)

    End Sub

    Private Sub RemoveUI()
        mSWApp.RemoveMenu(swDocumentTypes_e.swDocNONE, MenuItems.MenuName, Nothing)
        mSWApp.RemoveMenu(swDocumentTypes_e.swDocPART, MenuItems.MenuName, Nothing)
        mSWApp.RemoveMenu(swDocumentTypes_e.swDocASSEMBLY, MenuItems.MenuName, Nothing)

    End Sub

    Private Sub AddCommandManager()
        Dim commandGroup As CommandGroup

        If mBitmapHandler Is Nothing Then
            mBitmapHandler = New BitmapHandler()
        End If

        Dim thisAssembly As Assembly
        thisAssembly = Assembly.GetAssembly(Me.GetType())

        Dim commandIndex0 As Integer
        Dim commandIndex1 As Integer

        Dim docTypes() As Integer = {swDocumentTypes_e.swDocASSEMBLY, swDocumentTypes_e.swDocDRAWING, swDocumentTypes_e.swDocPART}

        Dim cmdGroupErr As Integer = 0
        Dim ignorePrevious As Boolean = False

        Dim registryIDs As Object = Nothing
        Dim getDataResult As Boolean = mCommandManager.GetGroupDataFromRegistry(Konstanten.MAIN_COMMAND_GROUP_ID, registryIDs)
        Dim knownIDs As Integer() = New Integer(1) {Konstanten.MAIN_ITEM_ID_1, Konstanten.MAIN_ITEM_ID_2}

        If getDataResult Then
            If Not CompareIDs(registryIDs, knownIDs) Then
                ignorePrevious = True
            End If
        End If

        commandGroup = mCommandManager.CreateCommandGroup2(Konstanten.MAIN_COMMAND_GROUP_ID, Konstanten.COMMAND_GROUP_TITLE, Konstanten.COMMAND_GROUP_TOOLTIP, "", -1, ignorePrevious, cmdGroupErr)
        If commandGroup Is Nothing Or thisAssembly Is Nothing Then
            Throw New NullReferenceException()
        End If

        Dim mainIcons = New String(1) {"NummernLasernLarge.png", "NummernLasernLarge.png"}
        Dim toolbarIcons = New String(1) {"NummernLasernLarge.png", "NummernLasernLarge.png"}
        commandGroup.IconList = toolbarIcons
        commandGroup.MainIconList = mainIcons

        commandIndex0 = commandGroup.AddCommandItem2(MenuItems.MenuItem1Name, -1, MenuItems.MenuItem1Hint, MenuItems.MenuItem1Tooltip, 1, "RunMenuCommand(" + MenuItems.MenuItem1Number.ToString + ")", "", Konstanten.MAIN_ITEM_ID_1, swCommandItemType_e.swToolbarItem)
        commandIndex1 = commandGroup.AddCommandItem2(MenuItems.MenuItem2Name, -1, MenuItems.MenuItem2Hint, MenuItems.MenuItem2Tooltip, 1, "RunMenuCommand(" + MenuItems.MenuItem2Number.ToString + ")", "", Konstanten.MAIN_ITEM_ID_2, swCommandItemType_e.swToolbarItem)

        commandGroup.HasToolbar = True
        commandGroup.HasMenu = True
        commandGroup.Activate()

        For Each docType As Integer In docTypes
            Dim cmdTab As ICommandTab = mCommandManager.GetCommandTab(docType, Konstanten.COMMAND_GROUP_TITLE)
            Dim bResult As Boolean

            If Not cmdTab Is Nothing And Not getDataResult Or ignorePrevious Then 'if tab exists, but we have ignored the registry info, re-create the tab.  Otherwise the ids won't matchup and the tab will be blank
                Dim res As Boolean = mCommandManager.RemoveCommandTab(cmdTab)
                cmdTab = Nothing
            End If

            If cmdTab Is Nothing Then
                cmdTab = mCommandManager.AddCommandTab(docType, Konstanten.COMMAND_GROUP_TITLE)

                Dim cmdBox As CommandTabBox = cmdTab.AddCommandTabBox

                Dim cmdIDs(2) As Integer
                Dim TextType(2) As Integer

                cmdIDs(0) = commandGroup.CommandID(commandIndex0)
                TextType(0) = swCommandTabButtonTextDisplay_e.swCommandTabButton_TextBelow

                cmdIDs(1) = commandGroup.CommandID(commandIndex1)
                TextType(1) = swCommandTabButtonTextDisplay_e.swCommandTabButton_TextBelow

                'cmdIDs(2) = commandGroup.ToolbarId
                'TextType(2) = swCommandTabButtonTextDisplay_e.swCommandTabButton_TextHorizontal

                bResult = cmdBox.AddCommands(cmdIDs, TextType)

                'Dim cmdBox1 As CommandTabBox = cmdTab.AddCommandTabBox()
                'ReDim cmdIDs(1)
                'ReDim TextType(1)

                'cmdIDs(0) = flyGroup.CmdID
                'TextType(0) = swCommandTabButtonTextDisplay_e.swCommandTabButton_TextBelow

                'bResult = cmdBox1.AddCommands(cmdIDs, TextType)

                'cmdTab.AddSeparator(cmdBox1, cmdIDs(0))

            End If
        Next

        thisAssembly = Nothing
    End Sub

    Public Sub RemoveCommandMgr()
        Try
            mBitmapHandler.Dispose()
            mCommandManager.RemoveCommandGroup(Konstanten.MAIN_COMMAND_GROUP_ID)
            'iCmdMgr.RemoveFlyoutGroup(flyoutGroupID)
        Catch e As Exception
        End Try
    End Sub

    Function CompareIDs(ByVal storedIDs() As Integer, ByVal addinIDs() As Integer) As Boolean

        Dim storeList As New List(Of Integer)(storedIDs)
        Dim addinList As New List(Of Integer)(addinIDs)

        addinList.Sort()
        storeList.Sort()

        If Not addinList.Count = storeList.Count Then

            Return False
        Else

            For i As Integer = 0 To addinList.Count - 1
                If Not addinList(i) = storeList(i) Then

                    Return False
                End If
            Next
        End If

        Return True
    End Function

#End Region

#Region "UI Callbacks"

    Private Sub RunMenuCommand(data As String)
        Select Case data
            Case MenuItems.MenuItem1Number.ToString
                mSWApp.SendMsgToUser("Nummern lasern geklickt")
                'NummernLasern.Main(mSWApp)
            Case MenuItems.MenuItem2Number.ToString
                mSWApp.SendMsgToUser("Dateieigenschaften")
        End Select
    End Sub

    Private Function SetMenuItemState(data As String) As Integer
        Select Case data
            Case MenuItems.MenuItem1Number.ToString
                Return 1
            Case MenuItems.MenuItem2Number.ToString
                Return 1
            Case Else
                Return 1
        End Select
    End Function

    Public Sub ButtonCallback()
        mSWApp.SendMsgToUser("Button callback")
    End Sub

#End Region

End Class
