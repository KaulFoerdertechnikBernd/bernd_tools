﻿Imports SolidWorks.Interop.sldworks
Imports SolidWorks.Interop.swconst

Public Class SolidWorksModel
    ''' <summary>
    ''' Checks the type of active model and returns ModelDoc2
    ''' </summary>
    ''' <param name="swApp">Current SolidWorks Object</param>
    ''' <param name="swDocType">Doctype as in swDocumentTypes_e</param>
    ''' <returns>ISldWorks::ModelDoc2 of the active document</returns>
    Shared Function GetActiveModel(swApp As SldWorks, swDocType As Integer)
        Dim swModel As ModelDoc2 = swApp.ActiveDoc
        If swModel Is Nothing Then
            swApp.SendMsgToUser2("Kein Model geladen!", swMessageBoxIcon_e.swMbInformation, swMessageBoxBtn_e.swMbOk)
        ElseIf swModel.GetType() <> swDocType Then
            If swDocType = swDocumentTypes_e.swDocPART Then
                swApp.SendMsgToUser2("Model ist kein Teil Dokument.", swMessageBoxIcon_e.swMbInformation, swMessageBoxBtn_e.swMbOk)
                swModel = Nothing
            ElseIf swDocType = swDocumentTypes_e.swDocASSEMBLY Then
                swApp.SendMsgToUser2("Model ist kein Baugruppen Dokument.", swMessageBoxIcon_e.swMbInformation, swMessageBoxBtn_e.swMbOk)
                swModel = Nothing
            Else
                swApp.SendMsgToUser2("Dokumenttyp wird nicht unterstützt. Nur Teile oder Baugruppen laden.", swMessageBoxIcon_e.swMbInformation, swMessageBoxBtn_e.swMbOk)
                swModel = Nothing
            End If
        End If
        Return swModel
    End Function
End Class
