﻿Public Class MenuItems

    Public Shared MenuName As String = "Kaul Tools"

    Public Shared MenuItem1Name As String = "Nummer einlasern"
    Public Shared MenuItem1Number As Integer = 1
    Public Shared MenuItem1Hint As String = "Dateinummer in das Blech einlasern"
    Public Shared MenuItem1Tooltip As String = "Dateinummer lasern"

    Public Shared MenuItem2Name As String = "Dateieigenschaften"
    Public Shared MenuItem2Number As Integer = 2
    Public Shared MenuItem2Hint As String = "Dateieigenschaftsfenster aufrufen und Daten ändern"
    Public Shared MenuItem2Tooltip As String = "Dateieigenschaften öffnen"

End Class
