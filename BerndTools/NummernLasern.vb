﻿Imports SolidWorks.Interop.sldworks
Imports SolidWorks.Interop.swconst

Public Class NummernLasern

#Region "Private Variables"
    Private Shared _PointPositionInSelection As Integer = -1
    Private Shared _EdgePositionInSelection As Integer = -1

    Private Shared _swModel As ModelDoc2 = Nothing
    Private Shared _swSelectionManager As SelectionMgr = Nothing
    Private Shared _swCustomPropertyManager As CustomPropertyManager = Nothing
    Private Shared _swMathUtility As MathUtility = Nothing
#End Region

    Shared Sub Main(swApp As SldWorks)
        _swModel = SolidWorksModel.GetActiveModel(swApp, swDocumentTypes_e.swDocPART)
        If _swModel Is Nothing Then Exit Sub

        If CheckSelection() = False Then Exit Sub
        If LaseredString() = Nothing Then Exit Sub

        _swMathUtility = swApp.GetMathUtility

        'Get face
        Dim swSelectedFace As Face2 = _swSelectionManager.GetSelectedObject6(_PointPositionInSelection, -1)

        'Get normal of face
        Dim normal As Object = swSelectedFace.Normal

        'Create vector from normal
        Dim yVector As MathVector = _swMathUtility.CreateVector(normal)

        'Get edge
        Dim swSelectedEdge As Edge = _swSelectionManager.GetSelectedObject6(_EdgePositionInSelection, -1)

        'Get closest point to click point from edge
        Dim clickedPoint As Object = _swSelectionManager.GetSelectionPoint2(_PointPositionInSelection, -1)
        Dim closestPointOnEdge As Object = swSelectedEdge.GetClosestPointOn(clickedPoint(0), clickedPoint(1), clickedPoint(2))

        'Create vector between click point and closest point
        Dim tempVectorClickedPoint As MathVector = _swMathUtility.CreateVector(clickedPoint)
        Dim tempVectorClosestPointOnEdge As MathVector = _swMathUtility.CreateVector(closestPointOnEdge)
        Dim zVector As MathVector = tempVectorClickedPoint.Subtract(tempVectorClosestPointOnEdge)

        'Normalise and get last vector
        'xVector is parallel to edge
        yVector = yVector.Normalise
        zVector = zVector.Normalise
        Dim xVector As MathVector = yVector.Cross(zVector)
        xVector = xVector.Normalise

        MsgBox("yVector: " + vbCrLf +
            yVector.ArrayData(0).ToString + vbCrLf +
            yVector.ArrayData(1).ToString + vbCrLf +
            yVector.ArrayData(2).ToString)

        MsgBox("zVector: " + vbCrLf +
            zVector.ArrayData(0).ToString + vbCrLf +
            zVector.ArrayData(1).ToString + vbCrLf +
            zVector.ArrayData(2).ToString)

        MsgBox("xVector: " + vbCrLf +
            xVector.ArrayData(0).ToString + vbCrLf +
            xVector.ArrayData(1).ToString + vbCrLf +
            xVector.ArrayData(2).ToString)

    End Sub

    ''' <summary>
    ''' Checks the user selection; Face and Edge is needed
    ''' </summary>
    ''' <returns>true or false</returns>
    Private Shared Function CheckSelection() As Boolean
        _swSelectionManager = _swModel.SelectionManager
        Dim NumberOfSelectedObjects As Integer = _swSelectionManager.GetSelectedObjectCount2(-1)

        'Face and edge is needed; two objects
        If NumberOfSelectedObjects = 2 Then
            Dim counter As Integer = 0
            For counter = 1 To NumberOfSelectedObjects
                If swSelectType_e.swSelEDGES = _swSelectionManager.GetSelectedObjectType3(counter, -1) Then
                    _EdgePositionInSelection = counter
                End If

                'Although I query the area I can get the clickpoint out of it
                If swSelectType_e.swSelFACES = _swSelectionManager.GetSelectedObjectType3(counter, -1) Then
                    _PointPositionInSelection = counter
                End If
            Next

            'Error handling for wrong selection by user
            'No face and edge were selected
            If _EdgePositionInSelection = -1 And _PointPositionInSelection = -1 Then
                MsgBox("Keine Fläche und Kante gewählt.")
                Return False

                'No edge was selected
            ElseIf _EdgePositionInSelection = -1 And _PointPositionInSelection > 0 Then
                MsgBox("Keine Kante gewählt.")
                Return False

                'No face was selected
            ElseIf _EdgePositionInSelection > 0 And _PointPositionInSelection = -1 Then
                MsgBox("Keine Fläche gewählt.")
                Return False

                'The selection is correct
            Else
                Return True
            End If

            'No selection was made
        ElseIf NumberOfSelectedObjects = 0 Then
            MsgBox("Keine Auswahl zu treffen ist nicht die Lösung. Versuche es erneut.")
            Return False

            'Wrong number of objects were selected
        Else
            MsgBox("Es müssen eine Kante und eine Fläche ausgewählt werden.")
            Return False
        End If
    End Function

    ''' <summary>
    ''' Gets the "Dateinummer" for feature creation; empty string if not present
    ''' </summary>
    ''' <returns>String</returns>
    Private Shared Function LaseredString() As String
        _swCustomPropertyManager = _swModel.Extension.CustomPropertyManager(Nothing)

        Dim ValOut As String = Nothing
        Dim ResolvedValOut As String = Nothing

        'Result code as defined in swCustomInfoGetResult_e
        'If you always want up-to-date data, then you should set UseCached to false
        Dim ResultCode As Integer = _swCustomPropertyManager.Get5(Konstanten.CUSTOM_PROPERTY_FOR_NUMBER_BLOCKS, False, ValOut, ResolvedValOut, False)

        If ResultCode = swCustomInfoGetResult_e.swCustomInfoGetResult_NotPresent Then
            MsgBox("Die Eigenschaft " + Chr(34) + Konstanten.CUSTOM_PROPERTY_FOR_NUMBER_BLOCKS + Chr(34) + " ist nicht vorhanden. Bitte anlegen.")
            Return Nothing
        ElseIf ResultCode = swCustomInfoGetResult_e.swCustomInfoGetResult_CachedValue Or ResultCode = swCustomInfoGetResult_e.swCustomInfoGetResult_ResolvedValue Then
            Return ValOut
        Else
            'This return should be unnecessary; just a precaution
            Return Nothing
        End If
    End Function

End Class
