﻿'Option Explicit On
'Const PI As Double = 3.14159265358979
'Const RadPerDeg As Double = PI / 180.0#

'Dim swMathutil As SldWorks.MathUtility

'Function Arccos(X As Double) As Double
'    If Abs(1.0# - X) < 0.00000001 Then Arccos = PI / 2.0# : Exit Function
'    Arccos = Atn(-X / Sqr(-X * X + 1)) + 2 * Atn(1)
'End Function

'Private Function CreateVector(p1 As Variant, p2 As Variant) As MathVector

'    Dim v1 As MathVector
'    Dim v2 As MathVector
'    Set v1 = swMathutil.CreateVector(p1)
'    Set v2 = swMathutil.CreateVector(p2)

'    Set CreateVector = v2.Subtract(v1)

'End Function

'Sub main()
'    Dim swApp As SldWorks.SldWorks

'    Dim swModel As SldWorks.ModelDoc2
'    Dim swPart As SldWorks.PartDoc


'    Dim swEdge As SldWorks.Edge
'    Dim swSelmgr As SldWorks.SelectionMgr
'    Dim selPt As Variant
'    Dim swFace As Face2
'    Dim skmanager As SketchManager
'    Dim swSketch As SldWorks.Sketch
'    Dim closestPointFace As Variant
'    Dim closestPointEdge As Variant
'    Dim normal As Variant
'    Dim zVec As MathVector
'    Dim yVec As MathVector
'    Dim xVec As MathVector
'    Dim swFeature As Feature


'    Dim skMgr As SldWorks.SketchManager
'    Dim pBlock As SldWorks.SketchBlockDefinition
'    Dim pInst As SldWorks.SketchBlockInstance
'    Dim insPt As SldWorks.MathPoint

'    Dim vInstances As Variant
'    Dim vBlocks As Variant
'    Dim nInstanceCount As Long

'    Dim jtr As Long

'    Dim nVector(2) As Double
'    Dim vVector As Variant
'    Set swApp = Application.SldWorks
'    Set swMathutil = swApp.GetMathUtility


'    Set swModel = swApp.ActiveDoc
'    Set swPart = swModel
'    Set skmanager = swModel.SketchManager
'    Set swSelmgr = swModel.SelectionManager
'    selPt = swSelmgr.GetSelectionPoint2(1, -1)
'    Dim p1 As MathPoint
'    Set p1 = swMathutil.CreatePoint(selPt)


'    Set swFace = swSelmgr.GetSelectedObject6(1, -1)
'    Set swEdge = swSelmgr.GetSelectedObject6(2, -1)
'    normal = swFace.normal
'    Set yVec = swMathutil.CreateVector(normal)

'    closestPointEdge = swEdge.GetClosestPointOn(selPt(0), selPt(1), selPt(2))
'    ReDim Preserve closestPointEdge(2)
'    Set zVec = CreateVector(selPt, closestPointEdge)
'    Set zVec = zVec.Normalise
'    Set yVec = yVec.Normalise
'    Set xVec = yVec.Cross(zVec)

'    Set xVec = xVec.Normalise

'    Dim selPointTransform As MathTransform

'    Set skMgr = swModel.SketchManager

'    Set selPointTransform = swMathutil.ComposeTransform(xVec, yVec, zVec, p1.ConvertToVector, 1)

'    Set xVec = xVec.Scale(0.01)

'    vBlocks = skMgr.GetSketchBlockDefinitions

'    If IsEmpty(vBlocks) Then
'        Exit Sub
'    End If
'    Dim blockCounter As Integer
'    For blockCounter = 0 To UBound(vBlocks)
'        Set pBlock = vBlocks(blockCounter)
'        Set insPt = pBlock.InsertionPoint

'        nInstanceCount = pBlock.GetInstanceCount

'        If nInstanceCount > 0 Then
'            vInstances = pBlock.GetInstances
'            For jtr = 0 To UBound(vInstances)
'                Set pInst = vInstances(jtr)
'                Set insPt = pInst.InstancePosition
'                Set swSketch = pInst.GetSketch
'                Dim sketchTransform As MathTransform

'                Dim nDotProd As Double


'                Set sketchTransform = swSketch.ModelToSketchTransform
'                Dim tt As MathTransform
'                Set tt = swMathutil.CreateTransform(Nothing)

'                Set swFeature = swSketch
'                swFeature.Select2 False, -1
'                swModel.EditSketch

'                pInst.InstancePosition = p1.MultiplyTransform(sketchTransform)
'                Set sketchTransform = sketchTransform.Inverse
'                 Dim sketchP1 As MathPoint
'                nVector(0) = sketchTransform.ArrayData(9) + sketchTransform.ArrayData(0)
'                nVector(1) = sketchTransform.ArrayData(10) + sketchTransform.ArrayData(1)
'                nVector(2) = sketchTransform.ArrayData(11) + sketchTransform.ArrayData(2)
'                vVector = nVector
'                Set sketchP1 = swMathutil.CreatePoint(vVector)

'                Dim sketchP2 As MathPoint
'                nVector(0) = sketchTransform.ArrayData(9)
'                nVector(1) = sketchTransform.ArrayData(10)
'                nVector(2) = sketchTransform.ArrayData(11)
'                vVector = nVector
'                Set sketchP2 = swMathutil.CreatePoint(vVector)

'                Dim sketchVec As MathVector
'                Set sketchVec = CreateVector(sketchP1.ArrayData, sketchP2.ArrayData)

'                Dim EdgeP1 As MathPoint
'                nVector(0) = selPointTransform.ArrayData(9) + selPointTransform.ArrayData(0)
'                nVector(1) = selPointTransform.ArrayData(10) + selPointTransform.ArrayData(1)
'                nVector(2) = selPointTransform.ArrayData(11) + selPointTransform.ArrayData(2)
'                vVector = nVector
'                Set EdgeP1 = swMathutil.CreatePoint(vVector)

'                Dim EdgeP2 As MathPoint
'                nVector(0) = selPointTransform.ArrayData(9)
'                nVector(1) = selPointTransform.ArrayData(10)
'                nVector(2) = selPointTransform.ArrayData(11)
'                vVector = nVector
'                Set EdgeP2 = swMathutil.CreatePoint(vVector)

'                Dim EdgeVec As MathVector
'                Set EdgeVec = CreateVector(EdgeP1.ArrayData, EdgeP2.ArrayData)
'                Set EdgeVec = EdgeVec.Normalise
'                Set sketchVec = sketchVec.Normalise
'                nDotProd = sketchVec.Dot(EdgeVec)
'                Dim crossVector As MathVector
'                Set crossVector = sketchVec.Cross(EdgeVec)
'                Set crossVector = crossVector.Normalise

'                If crossVector.Dot(yVec) < 0 Then
'                    pInst.Angle = 2 * PI - Arccos(nDotProd)
'                Else
'                    pInst.Angle = Arccos(nDotProd)
'                End If


'                swModel.InsertSketch2 True

'                Set p1 = p1.AddVector(xVec)
'            Next jtr
'        End If

'    Next blockCounter


'End Sub
