﻿Public Class Konstanten
    Public Const PATH_TO_BLOCKS As String = "d:\Kaul\Vorlagen\Blöcke\"
    Public Const PATH_TO_NUMBER_BLOCKS As String = "d:\Kaul\Vorlagen\Blöcke\Lasern\"
    Public Const CUSTOM_PROPERTY_FOR_NUMBER_BLOCKS As String = "Dateinummer"

    Public Const MAIN_COMMAND_GROUP_ID As Integer = 0
    Public Const MAIN_ITEM_ID_1 As Integer = 0
    Public Const MAIN_ITEM_ID_2 As Integer = 1
    Public Const COMMAND_GROUP_TITLE As String = "Kaul Tools"
    Public Const COMMAND_GROUP_TOOLTIP As String = "Alle Tools für den täglichen Gebrauch"
End Class
